#!/bin/bash

#-------------------------------------------------------------------------------
# AWS2Signer.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

EXECUTE=0
# CLIENT <curl | wget>
CLIENT="curl"
CURL_OPTS=""
WGET_OPTS=""
VERBOSE=0

function urlencode
{
    local y;y="$@";echo -n ${y/\\/\\\\} | while read -n1;do [[ $REPLY =~ [A-Za-z0-9_\.~\-] ]] && printf "$REPLY" || printf "%%%0X" \'"$REPLY";done;echo
}

source $AWS_CREDENTIAL_FILE
METHOD="POST"
SCHEME="https"
HOST="ec2.amazonaws.com"
PORT=""
SERVER_PATH="/"
if [[ $SCHEME = "http" && $PORT = "80" ]]; then
    SERVER=$HOST
elif [[ $SCHEME = "http" && "x_$PORT" = "x_" ]]; then
    SERVER=$HOST
elif [[ $SCHEME = "https" && $PORT = "443" ]]; then
    SERVER=$HOST
elif [[ $SCHEME = "https" && "x_$PORT" = "x_" ]]; then
    SERVER=$HOST
else
    SERVER="$HOST:$PORT"
fi

declare -A parameter
parameter["Timestamp"]=`date --utc +%Y-%m-%dT%H:%M:%SZ`
parameter["SignatureVersion"]="2"
parameter["SignatureMethod"]="HmacSHA256"
parameter["AWSAccessKeyId"]=$AWSAccessKeyId
parameter["Version"]="2012-07-20"
parameter["Action"]="DescribeAvailabilityZones"

readarray -t sorted < <(printf '%s\n' "${!parameter[@]}" | sort)

QUERY=""
FIRST=1
for key in "${sorted[@]}"
do
    if [ $key != "Signature" ]; then
        name=`urlencode "$key"`
        value=`urlencode "${parameter[$key]}"`
        if [ $FIRST -eq 1 ]; then
            FIRST=0
        else
            QUERY="$QUERY&"
        fi
        QUERY="$QUERY$name=$value"
    fi
done

STRING_TO_SIGN="$METHOD\n$SERVER\n$SERVER_PATH\n$QUERY"
if [ ${parameter["SignatureMethod"]} = "HmacSHA256" ]; then
    ALGORITHM="-sha256"
elif [ ${parameter["SignatureMethod"]} = "HmacSHA1" ]; then
    ALGORITHM="-sha1"
else
    echo "Unknown SignatureMethod: ${parameter["SignatureMethod"]}" 1>&2
    exit 1
fi
SIGNATURE=`echo -en $STRING_TO_SIGN | openssl dgst $ALGORITHM -hmac $AWSSecretKey -binary | openssl enc -base64`

QUERY="$QUERY&Signature=`urlencode "$SIGNATURE"`"

if [[ $CLIENT = "curl" || "x_$CLIENT" = "x_" ]]; then
    if [ $VERBOSE -ne 0 ]; then
        CURL_OPTS="$CURL_OPTS -v"
    fi

    if [ $METHOD = "POST" ]; then
        CMD="curl $CURL_OPTS -L \"$SCHEME://$SERVER$SERVER_PATH\" --request $METHOD --data \"$QUERY\""
    else
        CMD="curl $CURL_OPTS -L \"$SCHEME://$SERVER$SERVER_PATH?$QUERY\" --request $METHOD"
    fi
elif [ $CLIENT = "wget" ]; then
    if [ $VERBOSE -eq 0 ]; then
        WGET_OPTS="$WGET_OPTS -q"
    fi

    if [ $METHOD = "POST" ]; then
        CMD="wget $WGET_OPTS -O- --post-data \"$QUERY\" \"$SCHEME://$SERVER$SERVER_PATH\""
    elif [ $METHOD = "GET" ]; then
        CMD="wget $WGET_OPTS -O- \"$SCHEME://$SERVER$SERVER_PATH?$QUERY\""
    else
        echo "wget only does GET or POST. METHOD: $METHOD" 1>&2
        exit 1
    fi
else
    echo "Do not know how to use client: $CLIENT" 1>&2
    exit 1
fi

if [ $EXECUTE -ne 0 ]; then
    eval $CMD
else
    echo $CMD
fi

