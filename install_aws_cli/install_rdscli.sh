#!/bin/bash
#-------------------------------------------------------------------------------
# install_rdscli.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /opt/aws
curl --silent --output /tmp/aws/RDSCli.zip http://s3.amazonaws.com/rds-downloads/RDSCli.zip
rm -fR /tmp/aws/RDSCli-*
unzip -d /tmp/aws /tmp/aws/RDSCli.zip
rm -fR /opt/aws/RDSCli
mv /tmp/aws/RDSCli-* /opt/aws/RDSCli
rm -f /tmp/aws/RDSCli.zip
cat <<'EOF'>/etc/profile.d/RDSCli.sh
export AWS_RDS_HOME=/opt/aws/RDSCli
export PATH=$PATH:$AWS_RDS_HOME/bin
EOF
