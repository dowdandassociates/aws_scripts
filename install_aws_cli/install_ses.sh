#!/bin/bash
#-------------------------------------------------------------------------------
# install_ses.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /opt/aws
curl --silent --output /tmp/aws/ses-tools.zip http://d36cz9buwru1tt.cloudfront.net/catalog/attachments/ses-tools-2012-05-15.zip
rm -fR /tmp/aws/ses-tools
mkdir -p /tmp/aws/ses-tools
unzip -d /tmp/aws/ses-tools /tmp/aws/ses-tools.zip
rm -fR /opt/aws/ses-tools
mv /tmp/aws/ses-tools /opt/aws/ses-tools
rm -f /tmp/aws/ses-tools.zip
cat <<'EOF'>/etc/profile.d/ses-tools.sh
export AWS_SES_HOME=/opt/aws/ses-tools
export PATH=$PATH:$AWS_SES_HOME/bin
EOF

