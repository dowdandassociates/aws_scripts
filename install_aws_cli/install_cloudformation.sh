#!/bin/bash
#-------------------------------------------------------------------------------
# install_cloudformation.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /opt/aws
curl --silent --output /tmp/aws/AWSCloudFormation.zip https://s3.amazonaws.com/cloudformation-cli/AWSCloudFormation-cli.zip
rm -fR /tmp/aws/AWSCloudFormation-*
unzip -d /tmp/aws /tmp/aws/AWSCloudFormation.zip
rm -fR /opt/aws/AWSCloudFormation
mv /tmp/aws/AWSCloudFormation-* /opt/aws/AWSCloudFormation
rm -f /tmp/aws/AWSCloudFormation.zip
cat <<'EOF'>/etc/profile.d/AWSCloudFormation.sh
export AWS_CLOUDFORMATION_HOME=/opt/aws/AWSCloudFormation
export PATH=$PATH:$AWS_CLOUDFORMATION_HOME/bin
EOF

