#!/bin/bash
#-------------------------------------------------------------------------------
# install_sns.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /opt/aws
curl --silent --output /tmp/aws/SimpleNotificationServiceCli.zip http://sns-public-resources.s3.amazonaws.com/SimpleNotificationServiceCli-2010-03-31.zip
rm -fR /tmp/aws/SimpleNotificationServiceCli-*
unzip -d /tmp/aws /tmp/aws/SimpleNotificationServiceCli.zip
rm -fR /opt/aws/SimpleNotificationServiceCli
mv /tmp/aws/SimpleNotificationServiceCli-* /opt/aws/SimpleNotificationServiceCli
rm -f /tmp/aws/SimpleNotificationServiceCli.zip
cat <<'EOF'>/etc/profile.d/SimpleNotificationServiceCli.sh
export AWS_SNS_HOME=/opt/aws/SimpleNotificationServiceCli
export PATH=$PATH:$AWS_SNS_HOME/bin
EOF

